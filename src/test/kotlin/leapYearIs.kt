import org.junit.jupiter.api.Test

class leapYearIs {

    val leapYearcheck = leapYear()

    @Test
    fun Is2001NotLeapSoFalse(){

        assert(leapYearcheck.checkLeap(2001).equals(false))

    }
    @Test
    fun Is1996LeapSoTrue(){

        assert(leapYearcheck.checkLeap(1996).equals(true))

    }
    @Test
    fun Is1900NotLeapSoFalse(){

        assert(leapYearcheck.checkLeap(1900).equals(false))

    }
    @Test
    fun Is2000LeapSoTrue(){

        assert(leapYearcheck.checkLeap(2000).equals(true))

    }
}