fun main(args: Array<String>) {
    println("Hello World!")
}

class leapYear(){

    fun checkLeap(year :Int) :Boolean {

        return  when(true){
            year%400==0 -> true
            year%100==0 -> false
            year%4==0 -> true
            else -> false
        }
    }
}